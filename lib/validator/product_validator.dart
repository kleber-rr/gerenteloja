class ProductValidator {
  String validadeTitle(String text){
    if(text.isEmpty) return "Preencha o título do produto";
    return null;
  }

  String validateDescription(String text){
    if(text.isEmpty) return "Preencha a Descrição do produto";
    return null;
  }

  String validatePrice(String text){
    double price = double.tryParse(text);
    if(price == null) return "Preço inválido";
    if(!text.contains(".") || text.split(".")[1].length != 2) return "Utilize 2 casas decimais";
    return null;
  }

  String validateImages(List images){
    if(images.isEmpty) return "Adicione imagens do produto";
    return null;
  }
}